#!/bin/sh
set -x

SIGNER_NAME="${SIGNER_NAME:=thorchain}"
SIGNER_PASSWD="${SIGNER_PASSWD:=password}"
WALLET_PASSWD="thor"
BLOCK_TIME=${BLOCK_TIME:=3}
RPC_PORT=${RPC_PORT:=12345}

DCRCTL="dcrctl --notls"
export PATH=~/GolandProjects/mydcrwallet:$PATH

rm -rf ~/.dcrd
dcrd --notls --simnet --rpcuser=thor --rpcpass=thor --txindex &
sleep 5

rm -rf ~/.dcrwallet
/usr/bin/expect <<EOD
spawn dcrwallet --simnet --create  --pass=thor
expect "wallet encryption?"
send "y\r"
expect "public data?"
send "n\r"
expect "Do you have an existing wallet seed you want to use?"
send "y\r"
expect "(followed by a blank line):"
send "b280922d2cffda44648346412c5ec97f429938105003730414f10b01e1402eac\r\r"
expect "The wallet has been created successfully."
wait
EOD

sleep 2

WALLET_MINING_ADDR="SsXciQNTo3HuV5tX3yy4hXndRWgLMRVC7Ah"
WALLET_XFER_ADDR="SsWKp7wtdTZYabYFYSc9cnxhwFEjA5g4pFc" # same as above

#killall dcrwallet
killall dcrd


dcrwallet --noclienttls --noservertls --simnet --username=thor --password=thor --pass=thor  &
sleep 2
echo "getting new adress..."
dcrctl --notls --simnet --wallet --rpcuser=thor --rpcpass=thor getnewaddress
sleep 2


#rm -rf ~/.dcrd
dcrd --notls --simnet --rpcuser=thor --rpcpass=thor --miningaddr=${WALLET_MINING_ADDR} --txindex &

sleep 3
echo "mining 64 blocks to fund master address..."
while true
do
	dcrctl --notls --simnet --rpcuser=thor --rpcpass=thor generate 64 && break
done

sleep 2
echo "creating account user1..."

dcrctl --notls --simnet --rpcuser=thor --rpcpass=thor --wallet createnewaccount user1
USER1_ADDR=$(dcrctl --notls --simnet --rpcuser=thor --rpcpass=thor --wallet getaccountaddress user1)
dcrctl --notls --simnet --wallet --rpcuser=thor --rpcpass=thor getbalance user1
dcrctl --notls --simnet --rpcuser=thor --rpcpass=thor --wallet sendtoaddress ${USER1_ADDR} 100
dcrctl --notls --simnet --rpcuser=thor --rpcpass=thor generate 10
dcrctl --notls --simnet --wallet --rpcuser=thor --rpcpass=thor getbalance user1
sleep 5

dcrctl --notls --simnet --wallet --rpcuser=thor --rpcpass=thor sendfrom user1 SseoiUzpnLQ7vqYLPXeN1Ke3nXKxKvN4Van 6.9
dcrctl --notls --simnet --rpcuser=thor --rpcpass=thor generate 10
dcrctl --notls --simnet --wallet --rpcuser=thor --rpcpass=thor sendfrom user1 SsYCg4f1cANHzgeepqfiXydJgeWv3jiweWP 9.6
dcrctl --notls --simnet --rpcuser=thor --rpcpass=thor generate 10
dcrctl --notls --simnet --wallet --rpcuser=thor --rpcpass=thor sendfrom user1 SsYCg4f1cANHzgeepqfiXydJgeWv3jiweWP 1.2
dcrctl --notls --simnet --rpcuser=thor --rpcpass=thor generate 10

sleep 2
dcrctl --notls --simnet --wallet --rpcuser=thor --rpcpass=thor getbalance user1
sleep 2


while true
do
	dcrctl --notls --simnet --rpcuser=thor --rpcpass=thor generate 1
    dcrctl --notls --simnet --wallet --rpcuser=thor --rpcpass=thor getbalance user1
	sleep $BLOCK_TIME
done

