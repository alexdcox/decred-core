#!/bin/sh
set -x

SIGNER_NAME="${SIGNER_NAME:=thorchain}"
SIGNER_PASSWD="${SIGNER_PASSWD:=password}"
WALLET_PASSWD="thor"
BLOCK_TIME=${BLOCK_TIME:=3}

DCRCTL="dcrctl "

rm -rf ~/.dcrd
dcrd --txindex  --simnet --rpcuser=${SIGNER_NAME}  --rpcpass=${SIGNER_PASSWD} &
sleep 5

rm -rf ~/.dcrwallet
/usr/bin/expect <<EOD
spawn dcrwallet --simnet --create  --pass=${SIGNER_PASSWD}
expect "wallet encryption?"
send "y\r"
expect "public data?"
send "n\r"
expect "Do you have an existing wallet seed you want to use?"
send "y\r"
expect "(followed by a blank line):"
send "b280922d2cffda44648346412c5ec97f429938105003730414f10b01e1402eac\r\r"
expect "The wallet has been created successfully."
wait
EOD

sleep 2

WALLET_MINING_ADDR="SsXciQNTo3HuV5tX3yy4hXndRWgLMRVC7Ah"
WALLET_XFER_ADDR="SsWKp7wtdTZYabYFYSc9cnxhwFEjA5g4pFc" # same as above

#killall dcrwallet
killall dcrd


dcrwallet --nogrpc  --simnet --username=${SIGNER_NAME} --password=${SIGNER_PASSWD} --pass=${SIGNER_PASSWD}  --rpclisten=172.17.0.2 --rpclisten=127.0.0.1 &
sleep 2
echo "getting new adress..."
dcrctl  --simnet --wallet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} getnewaddress
sleep 10


#rm -rf ~/.dcrd
dcrd --txindex  --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --miningaddr=${WALLET_MINING_ADDR} &

sleep 3
echo "mining 64 blocks to fund master address..."
while true
do
	dcrctl   --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} generate 64 && break
done

sleep 2
echo "creating account user1..."

dcrctl  --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --wallet createnewaccount user1
USER1_ADDR=$(dcrctl  --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --wallet getaccountaddress user1)
dcrctl  --simnet --wallet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} getbalance user1
dcrctl  --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --wallet sendtoaddress ${USER1_ADDR} 97.65
dcrctl   --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} generate 10
dcrctl  --simnet --wallet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} getbalance user1
sleep 5

dcrctl  --simnet --wallet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} sendfrom user1 ${WALLET_MINING_ADDR} 17.00
dcrctl   --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} generate 10
sleep 5
dcrctl  --simnet --wallet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} getbalance user1
dcrctl  --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --wallet sendtoaddress ${USER1_ADDR} 100
dcrctl  --simnet --wallet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} sendfrom user1 ${WALLET_MINING_ADDR} 17.00

sleep 2
dcrctl  --simnet --wallet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} getbalance user1
sleep 2
echo "funding user1"


while true
do
	dcrctl   --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} generate 1
	sleep $BLOCK_TIME
	dcrctl  --simnet --wallet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} getbalance user1
done

