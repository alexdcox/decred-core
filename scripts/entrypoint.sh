#!/bin/sh
set -e

if [ $(echo "$1" | cut -c1) = "-" ]; then
  echo "$0: assuming arguments for dcrd"

  set -- dcrd "$@"
fi

if [ $(echo "$1" | cut -c1) = "-" ] || [ "$1" = "dcrd" ]; then
  mkdir -p "$DECRED_DATA"
  chmod 700 "$DECRED_DATA"
  chown -R decred "$DECRED_DATA"

  echo "$0: setting data directory to $DECRED_DATA"

  set -- "$@" -datadir="$DECRED_DATA"
fi

if [ "$1" = "dcrd" ] || [ "$1" = "dcrctl" ] || [ "$1" = "dcrwallet" ]; then
  echo
  exec su-exec decred "$@"
fi

echo
exec "$@"
