#!/bin/sh
set -x

SIGNER_NAME="${SIGNER_NAME:=thorchain}"
SIGNER_PASSWD="${SIGNER_PASSWD:=password}"
WALLET_PASSWD="thor"
BLOCK_TIME=${BLOCK_TIME:=3}

DCRCTL="dcrctl --skipverify"

rm -rf ~/.dcrd
dcrd --txindex  --simnet --rpcuser=${SIGNER_NAME}  --rpcpass=${SIGNER_PASSWD} &
sleep 5

rm -rf ~/.dcrwallet
/usr/bin/expect <<EOD
spawn dcrwallet --simnet --create  --pass=${SIGNER_PASSWD}
expect "wallet encryption?"
send "y\r"
expect "public data?"
send "n\r"
expect "Do you have an existing wallet seed you want to use?"
send "y\r"
expect "(followed by a blank line):"
send "b280922d2cffda44648346412c5ec97f429938105003730414f10b01e1402eac\r\r"
expect "The wallet has been created successfully."
wait
EOD

sleep 2

WALLET_MINING_ADDR="SsXciQNTo3HuV5tX3yy4hXndRWgLMRVC7Ah"
WALLET_XFER_ADDR="SsWKp7wtdTZYabYFYSc9cnxhwFEjA5g4pFc" # same as above

#killall dcrwallet
killall dcrd


sleep 5

dcrd  --txindex  --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --miningaddr=${WALLET_MINING_ADDR} &

sleep 3
echo "mining 32 blocks to fund master address..."
while true
do
	${DCRCTL}   --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} generate 32 && break
done
sleep 2

# this must be later than the mining. 
dcrwallet --nogrpc --noservertls  --simnet --username=${SIGNER_NAME} --password=${SIGNER_PASSWD} --pass=${SIGNER_PASSWD} --rpclisten=$(grep $(hostname) /etc/hosts | cut -f 1) --rpclisten=127.0.0.1 &
sleep 10

${DCRCTL}  --notls --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --wallet getaddressesbyaccount default
echo "creating account user1..."
${DCRCTL}  --notls --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --wallet createnewaccount user1
USER1_ADDR=$(${DCRCTL}  --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --notls --wallet getaccountaddress user1)
sleep 5
${DCRCTL}  --notls --simnet --wallet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} getbalance 
${DCRCTL}  --notls --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --wallet sendtoaddress ${USER1_ADDR} 97.65 "fund1"
${DCRCTL}   --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} generate 16
sleep 3
${DCRCTL}  --notls  --simnet --wallet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} listunspent 1 10000000 \[\"${USER1_ADDR}\"\] user1
${DCRCTL}  --notls --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} --wallet getaddressesbyaccount user1
sleep 5


while true
do
	${DCRCTL}   --simnet --rpcuser=${SIGNER_NAME} --rpcpass=${SIGNER_PASSWD} generate 1
	sleep $BLOCK_TIME
done

